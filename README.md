# Coterie Craft (MineClone 2 edition)

![Logo](textures/menu_header.png)

In this repository you can find converted Coterie Craft texture pack (by Cpt. Corn, Ganlolde, karst and others) for MineClone 2. Some work on converting was done by the texture converter from MCL2's tools, some work was manual and done by me.

From the original forum topic ([link](https://www.minecraftforum.net/topic/115061-16x-coterie-craft-v5s02-mc174-and-below-updated-21014/)): *Coterie Craft is a "neo-faithful" texture pack determined to bring Minecraft the textures it deserves, while preserving the iconic feel of default's textures.*

## License

[![CC-BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
